import 'contract_model.dart';

class ContractsModel {
  List<ContractModel> contracts =[];
  ContractsModel.fromProvider(List<dynamic> list) {
    List<ContractModel> temp =[];
    list.forEach((element) {

      if (element != null) {
        ContractModel newContract = new ContractModel(
            element['calificacion'],
            element['cliente'],
            element['worker'],
            element['dia'],
            element['hora'],
            element['medio_de_pago'],
            element['precio'],
            element['latitud'],
            element['longitud'],
            element['service'],
            element['complaint']);

        temp.add(newContract);
      }
    });

    contracts = temp;
  }
}
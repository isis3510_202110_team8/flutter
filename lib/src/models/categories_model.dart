import 'package:house_holdd/src/models/contract_model.dart';
import 'package:house_holdd/src/models/worker_model.dart';
import 'package:house_holdd/src/models/category_model.dart';

class CategoriesModel {
  List<CategorieModel> _categories = [];
  List<CategorieModel> _category = [];

  CategoriesModel.fromProvider(Map<dynamic, dynamic> map) {
    List<CategorieModel> temp = [];
    String name = '';
    map.forEach((key, value) {
      List<WorkerModel> categoryWorkers = [];
      Map<dynamic, dynamic> data = value;
      data.forEach((key, value) {
        if (key == 'nombre') {
          name = value;
        } else {
          List<dynamic> workers = value;
          workers.forEach((element) {
            List<ContractModel> workerContracts = [];
            if (element['contratos'] != null) {
              List<dynamic> wContracts = element['contratos'];
              wContracts.forEach((element) {
                ContractModel contractModel = new ContractModel(
                    element['calificacion'],
                    element['cliente'],
                    element['worker'],
                    element['dia'],
                    element['hora'],
                    element['medio_de_pago'],
                    element['precio'],
                    element['latitud'],
                    element['longitud'],
                    element['service'],
                    element['complaint']);
                workerContracts.add(contractModel);
              });
              WorkerModel worker = new WorkerModel.fromProvider(
                element['image_url'],
                  element['category'],
                  workerContracts,
                  element['edad'],
                  element['email'],
                  element['first_name'],
                  element['longitud'],
                  element['latitud'],
                  element['password'],
                  element['phone'],
                  element['covid_symptoms'],
                  element['services']);
              categoryWorkers.add(worker);
            } else {
              WorkerModel worker = new WorkerModel.fromProvider(
                  element['image_url'],
                  element['category'],
                  [],
                  element['edad'],
                  element['email'],
                  element['first_name'],
                  element['longitud'],
                  element['latitud'],
                  element['password'],
                  element['phone'],
                  element['covid_symptoms'],
                  element['services']);
              categoryWorkers.add(worker);
            }
          });
        }
      });
      CategorieModel newCategorie =
          new CategorieModel(name.toLowerCase(), name, categoryWorkers);
      temp.add(newCategorie);
    });
    _categories = temp;
  }

  CategoriesModel.justName(Map<dynamic, dynamic> map) {
    List<CategorieModel> temp = [];
    List<WorkerModel> categoryWorkers = [];
    map.forEach((key, value) {
      List categories = value;
      categories.forEach((element) {
        String name = element;
        CategorieModel newCategorie =
            new CategorieModel(name.toLowerCase(), name, categoryWorkers);
        temp.add(newCategorie);
      });
    });
    _categories = temp;
  }

  CategoriesModel.singleCategory(Map<dynamic, dynamic> map) {
    List<CategorieModel> temp = [];
    String name = '';
    List<WorkerModel> categoryWorkers = [];
    map.forEach((key, value) {
      if (key == 'nombre') {
        name = value;
      } else {
        List<dynamic> workers = value;
        workers.forEach((element) {
          if (element['contratos'] != null) {
            List<dynamic> wContracts = element['contratos'];

            List<ContractModel> workerContracts = [];
            wContracts.forEach((element) {
              ContractModel contractModel = new ContractModel(
                  element['calificacion'],
                  element['cliente'],
                  element['worker'],
                  element['dia'],
                  element['hora'],
                  element['medio_de_pago'],
                  element['precio'],
                  element['latitud'],
                  element['longitud'],
                  element['service'],
                  element['complaint']);
              workerContracts.add(contractModel);

            });
            WorkerModel worker = new WorkerModel(
                element['image_url'],
                element['category'],
                workerContracts,
                element['edad'],
                element['email'],
                element['first_name'],
                element['longitud'],
                element['latitud'],
                element['password'],
                element['phone'],
                element['covid_symptoms'],
                element['services']);
            categoryWorkers.add(worker);

          } else {
            WorkerModel worker = new WorkerModel(
                element['image_url'],
                element['category'],
                [],
                element['edad'],
                element['email'],
                element['first_name'],
                element['longitud'],
                element['latitud'],
                element['password'],
                element['phone'],
                element['covid_symptoms'],
                element['services']);
            categoryWorkers.add(worker);

          }
        });
      }
    });
    CategorieModel newCategorie =
        new CategorieModel(name.toLowerCase(), name, categoryWorkers);
    temp.add(newCategorie);
    _category = temp;
  }

  List<CategorieModel> get categories => _categories;

  List<CategorieModel> get category => _category;
}

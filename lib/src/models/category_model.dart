import 'package:house_holdd/src/models/worker_model.dart';

class CategorieModel {
  String _lowerName;
  String _name;
  List<WorkerModel> _workers = [];

  CategorieModel(pLowerName, pName, pWorkers) {
    _lowerName = pLowerName;
    _name = pName;
    _workers = pWorkers;
  }
  String get name => _name;

  String get lowerName => _lowerName;

  List<WorkerModel> get workers => _workers;

}
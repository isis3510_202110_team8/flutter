import 'package:house_holdd/src/models/contract_model.dart';

class WorkerModel {
  String image_url;
  String category;
  List<ContractModel> contratos;
  int age;
  String email;
  String firstName;
  double longitud;
  double latitud;
  String password;
  String phone;
  bool covid_symptoms;
  List services;
  WorkerModel(
      String pImage_url,
      String pCategory,
      List<ContractModel> pContratos,
      int pAge,
      String pEmail,
      String pFirstName,
      double pLongitud,
      double pLatitud,
      String pPassword,
      String pPhone,
      bool pCovid_symptoms,
      List pServices) {
    image_url=pImage_url;
    category = pCategory;
    contratos = pContratos;
    age = pAge;
    email = pEmail;
    firstName = pFirstName;
    longitud = pLongitud;
    latitud = pLatitud;
    password = pPassword;
    phone = pPhone;
    covid_symptoms=pCovid_symptoms;
    services = pServices;
  }

  WorkerModel.fromProvider(
      String pImage_url,
      String pCategory,
      List<ContractModel> pContratos,
      int pAge,
      String pEmail,
      String pFirstName,
      double pLongitud,
      double pLatitud,
      String pPassword,
      String pPhone,
      bool pCovid_symptoms,
      List pServices) {
    print(pCovid_symptoms.runtimeType);
    WorkerModel newW = new WorkerModel(pImage_url,pCategory, pContratos, pAge, pEmail,
        pFirstName, pLongitud, pLatitud, pPassword, pPhone,pCovid_symptoms,pServices);
  }
}

import 'package:house_holdd/src/models/user_model.dart';

class UsersModel {
  List<UserModel> users =[];
  UsersModel.fromProvider(List<dynamic> list) {
    List<UserModel> temp =[];
    list.forEach((element) {
      if (element != null) {
        UserModel newUser = new UserModel(
            element['latitud'],
            element['longitud'],
            element['password'],
            element['fecha_registro'],
            element['genero'],
            element['last_name'],
            element['contratos'],
            element['edad'],
            element['first_name'],
            element['email']);
       temp.add(newUser);
      }
    });
    users = temp;
  }
}

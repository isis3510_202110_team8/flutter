class UserModel {
  double latitud;
  double longitud;
  String password;
  String dateRegistration;
  String gender;
  String lastName;
  List contracts;
  int age;
  String firstName;
  String email;

  UserModel(
      double pLatitud,
      double pLongitud,
      String pPassword,
      String pFechaRegistro,
      String pGenero,
      String pLastName,
      List pContratos,
      int pEdad,
      String pFirstName,
      String pEmail) {
    latitud = pLatitud;
    longitud = pLongitud;
    password = pPassword;
    dateRegistration = pFechaRegistro;
    gender = pGenero;
    lastName = pLastName;
    contracts = pContratos;
    age = pEdad;
    firstName = pFirstName;
    email = pEmail;
  }
}

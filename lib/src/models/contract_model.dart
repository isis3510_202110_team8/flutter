import 'package:flutter/material.dart';

class ContractModel {
  int calification;
  String clientEmail;
  String workerEmail;
  var day;
  TimeOfDay hour;
  String payMethod;
  int price;
  double latitud;
  double longitud;
  String service;
  String complaint;


  ContractModel(int pCalification,
      String pClientEmail,
      String pWorkerEmail,
      String pDay,
      String pHour,
      String pPayMethod,
      int pPrice,
      double pLatitud,
      double pLongitud,
      String pService,
      String pComplaint) {


    calification = pCalification;
    clientEmail = pClientEmail;
    workerEmail = pWorkerEmail;


    List mesDiaAnoFecha = pDay.split("/");

    day = DateTime.utc(int.parse(mesDiaAnoFecha[2]), int.parse(mesDiaAnoFecha[1]), int.parse(mesDiaAnoFecha[0]));

    List dataHour = pHour.split(" ");
    List horaMinuto = dataHour[0].split(":");
    int newHour = int.parse(horaMinuto[0]);
    int minutes = int.parse(horaMinuto[1]);


    if (dataHour[1] == "PM") {
      newHour = int.parse(horaMinuto[0]) + 12;
    }
    hour = TimeOfDay(hour:newHour, minute: minutes);
    payMethod = pPayMethod;
    price = pPrice;
    latitud = pLatitud;
    longitud = pLongitud;
    service = pService;
    complaint = pComplaint;

  }

}

import 'package:firebase_database/firebase_database.dart';
import 'package:house_holdd/src/models/worker_model.dart';

class WorkersProvider{

  Future<WorkerModel> getWorker (int worker) async {
    final databaseReference = FirebaseDatabase.instance.reference().child("workers").child(worker.toString());
    DataSnapshot info;
    await databaseReference.once().then((DataSnapshot snapshot) {
      info = snapshot;
    });
    return WorkerModel.fromProvider(info.value["image_url"],info.value["category"].toString(),info.value["contratos"],int.parse(info.value["edad"].toString()),info.value["email"],info.value["first_name"],double.parse(info.value["longitud"].toString()),double.parse(info.value["latitud"].toString()),info.value["password"],info.value["phone"], info.value["covid_symptoms"],info.value["services"]);
  }
}
import 'package:firebase_database/firebase_database.dart';

class ProposalProvider{


  Future<void> insertProposal (String userEmail, String dateTime, String hour, double latitude, double longitude, String payment_method, String price, String service, String workerEmail) async {
    final databaseReference = FirebaseDatabase.instance.reference().child("proposals");
    await databaseReference.push().set({
      'cliente':userEmail,
      'dia':dateTime,
      'hora':hour,
      'latitud':latitude,
      'longitud':longitude,
      'medio_de_pago':payment_method,
      'price':price,
      'service':service,
      'worker':workerEmail,
    });

  }
}
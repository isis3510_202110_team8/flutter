import 'package:firebase_database/firebase_database.dart';
import 'package:house_holdd/src/models/contracts_model.dart';

class ContractsProvider{

  Future<ContractsModel> getContracts () async {
    final databaseReference = FirebaseDatabase.instance.reference().child("contratos");
    DataSnapshot info;

    await databaseReference.once().then((DataSnapshot snapshot) {
      info = snapshot;
    });
    return ContractsModel.fromProvider(info.value);

  }
}

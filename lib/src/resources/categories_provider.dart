import 'package:firebase_database/firebase_database.dart';
import '../models/categories_model.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class CategoriesProvider{

  Future<CategoriesModel> getCategories () async {
    String result = await rootBundle.loadString('assets/json/Categories.json');
    Map _json = json.decode(result);
    return CategoriesModel.justName(_json);
  }
  Future<CategoriesModel> getCategorie (category) async {
    final databaseReference = FirebaseDatabase.instance.reference().child("categories").child(category);
    DataSnapshot info;
    await databaseReference.once().then((DataSnapshot snapshot) {
      info = snapshot;
    });
    return CategoriesModel.singleCategory(info.value);
  }

}
import 'package:firebase_database/firebase_database.dart';
import 'package:house_holdd/src/models/users_model.dart';

class UsersProvider{

  Future<UsersModel> getUsers () async {
    final databaseReference = FirebaseDatabase.instance.reference().child("users");
    DataSnapshot info;
    await databaseReference.once().then((DataSnapshot snapshot) {
      info = snapshot;
    });
    return UsersModel.fromProvider(info.value);

  }
  Future<void> insertUser (String first_name, String last_name, String email, int age, String gender, double longitude, double latitude, String date) async {
    final databaseReference = FirebaseDatabase.instance.reference().child("users");
    await databaseReference.push().set({
      'contratos':[],
      'edad':age,
      'email':email,
      'fecha_registro':date,
      'first_name':first_name,
      'genero':gender,
      'last_name':last_name,
      'latitud':latitude,
      'longitud':longitude,
    });

  }
}
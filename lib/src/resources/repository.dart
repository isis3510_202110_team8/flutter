import 'dart:async';
import 'package:house_holdd/src/models/categories_model.dart';
import 'package:house_holdd/src/models/contracts_model.dart';
import 'package:house_holdd/src/models/users_model.dart';
import 'package:house_holdd/src/models/worker_model.dart';
import 'package:house_holdd/src/resources/contracts_provider.dart';
import 'package:house_holdd/src/resources/proposal_provider.dart';
import 'package:house_holdd/src/resources/users_provider.dart';
import 'package:house_holdd/src/resources/workers_provider.dart';
import 'categories_provider.dart';
class Repository {

  final categoriesProvider = CategoriesProvider();
  final usersProvider = UsersProvider();
  final contractsProvider =ContractsProvider();
  final workersProvider = WorkersProvider();
  final proposalProvider = ProposalProvider();

  Future<CategoriesModel> fetchAllCategories()=>categoriesProvider.getCategories();
  Future<CategoriesModel> fetchACategorie(category)=>categoriesProvider.getCategorie(category);
  Future<UsersModel> fetchAllUsers()=>usersProvider.getUsers();
  Future<void> insertUser(String first_name, String last_name, String email, int age, String gender, double longitude, double latitude, String date)=>usersProvider.insertUser(first_name, last_name,  email,  age,  gender,  longitude,  latitude,  date);
  Future<ContractsModel> fetchAllContracts()=>contractsProvider.getContracts();
  Future<WorkerModel> fetchWorker(worker)=> workersProvider.getWorker(worker);
  Future<void> insertProposal(String userEmail, String dateTime, String hour, double latitude, double longitude, String payment_method, String price, String service, String workerEmail)=>proposalProvider.insertProposal(userEmail, dateTime, hour, latitude, longitude, payment_method, price, service, workerEmail);

}
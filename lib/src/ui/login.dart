import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:house_holdd/main.dart';
import 'package:house_holdd/src/ui/signup.dart';
import 'package:house_holdd/src/blocs/login_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:house_holdd/src/resources/connectivity_provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final fontColor = Color.fromRGBO(59, 89, 152, 5);
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();

  showDialog(mensaje){
    showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(mensaje),
          actions: [
            CupertinoDialogAction(
                child: Text('Close'),
                onPressed: () => Navigator.pop(context)
            ),
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Container(
                  width: 200,
                  height: 80,
                  child: Text(
                    'HouseChores',
                    style: TextStyle(
                        fontFamily: 'PlayfairDisplay',
                        color: fontColor,
                        fontSize: 30),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                controller: _email,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email id as abc@gmail.com'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 15),
              child: TextField(
                controller: _password,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password'),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: fontColor, borderRadius: BorderRadius.circular(5)),
              child: TextButton(
                onPressed: () async {
                  bool isConection = await Connectivity_provider().isConetion();
                  if (isConection) {
                    loginBloc
                        .singIn(_email.text, _password.text)
                        .then((value) async {
                      await SharedPreferences.getInstance().then((prefs) {
                        String resp = prefs.get('isUser');
                        if (resp == "") {
                          FirebaseAuth _auth = FirebaseAuth.instance;
                          resp = _auth.currentUser.email;
                          prefs.setString('isUser', resp);
                        }
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => MyApp(user: resp)));
                      });
                    }).catchError((err){
                      showDialog(err.message);
                    });
                  } else {
                    showDialog("No Internet connection. Make sure that Wi-Fi or mobile data is turned on, then try again." );
                  }
                },
                child: Text(
                  'Log In',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: fontColor, borderRadius: BorderRadius.circular(5)),
              child: TextButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => SignUp()));
                },
                child: Text(
                  'Sign Up',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:house_holdd/src/blocs/detail_bloc.dart';
import 'package:house_holdd/src/models/category_model.dart';
import 'package:house_holdd/src/ui/worker.dart';



class Detail extends StatelessWidget {
  CategorieModel category;

  Detail({this.category});

  @override
  Widget build(BuildContext context) {
    Widget bestW(workers, best, num, texto) {
      return Container(
        padding: EdgeInsets.only(top: 10, bottom: 15),
        child: Column(
          children: [
            Text(
              texto,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
            ),
            SizedBox(height: 5),
            ListView.builder(
              itemCount: best,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                List a = workers[num][index];
                return ListTile(
                    leading: CachedNetworkImage(
                      imageUrl: a[7],
                      width: 50,
                      height: 50,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                    title: Text(a[0]),
                    subtitle: Text(
                        a[1] + "\n" + "Distance :" + a[8].toString() + " km"),
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => WorkerUI(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7],a[8],a[9]))));
              },
            ),
          ],
        ),
      );
    }

    DetailBloc detailBloc = new DetailBloc(this.category.name.toLowerCase());
    return Scaffold(
        appBar: AppBar(
          title: Text(this.category.name),
        ),
        body: StreamBuilder(
            stream: detailBloc.getWorkers,
            builder: (_, AsyncSnapshot snapshot) {
              final workers = snapshot.data ?? [];
              int recommended = 0;
              int best = 0;
              int workersTamanio = 0;
              if (workers != null) {
                if (workers.length == 3) {
                  recommended = workers[0].length;
                  best = workers[1].length;
                  workersTamanio = workers[2].length;
                } else if (workers.length == 1) {
                  return Center(child: Text(workers[0]));
                } else {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(
                          semanticsLabel: "Loading",
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.blue),
                        ),
                        SizedBox(height: 30),
                        Text("Loading workers")
                      ],
                    ),
                  );
                }
              }
              if (recommended > 0 && best > 0) {
                return Column(children: [
                  bestW(workers, recommended, 0, "Recommended for you"),
                  bestW(workers, best, 1, "Best Workers"),
                  bestW(workers, workersTamanio, 2, "Workers"),
                ]);
              } else if (best > 0) {
                return Column(children: [
                  bestW(workers, best, 1, "Best Workers"),
                  bestW(workers, workersTamanio, 2, "Workers"),
                ]);
              } else if (recommended > 0) {
                return Column(children: [
                  bestW(workers, recommended, 0, "Recommended for you"),
                  bestW(workers, workersTamanio, 2, "Workers"),
                ]);
              } else {
                return Column(children: [
                  bestW(workers, workersTamanio, 2, "Workers"),
                ]);
              }
            }));
  }
}

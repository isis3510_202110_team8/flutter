import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:house_holdd/src/resources/connectivity_provider.dart';
import 'package:intl/intl.dart';
import 'package:house_holdd/src/blocs/proposal_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:house_holdd/main.dart';

class Proposal extends StatefulWidget {
  String name;
  String email;
  int age;
  double longitude;
  double latitude;
  String phone;
  bool covid_sympthoms;
  String image_url;
  double distance;
  List<dynamic> services;

  Proposal(
      this.name,
      this.email,
      this.age,
      this.longitude,
      this.latitude,
      this.phone,
      this.covid_sympthoms,
      this.image_url,
      this.distance,
      this.services);

  @override
  _ProposalState createState() => _ProposalState();
}

class _ProposalState extends State<Proposal> {
  String service;

  String payment_method = 'Tarjeta';

  String _setTime, _setDate;

  String _hour, _minute, _time;

  String dateTime;

  DateTime selectedDate = DateTime.now();

  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);

  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();

  TextEditingController _price = TextEditingController();

  _showDialog(mensaje) {
    showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
              title: Text(mensaje),
              actions: [
                CupertinoDialogAction(
                    child: Text('Close'),
                    onPressed: () => Navigator.pop(context)),
              ],
            ));
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        selectedDate = picked;
        _dateController.text = DateFormat.yMd().format(selectedDate);
      });
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
        _hour = selectedTime.hour.toString();
        _minute = selectedTime.minute.toString();
        _time = _hour + ' : ' + _minute;
        _timeController.text = _time;
        _timeController.text = formatDate(
            DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  @override
  void initState() {
    service = widget.services[0].toString();
    _dateController.text = DateFormat.yMd().format(DateTime.now());

    _timeController.text = formatDate(
        DateTime(2019, 08, 1, DateTime.now().hour, DateTime.now().minute),
        [hh, ':', nn, " ", am]).toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    dateTime = DateFormat.yMd().format(DateTime.now());
    return Scaffold(
        appBar: AppBar(
          title: Text("Contract Service Proposal"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                child: ListTile(
                  title: Text('Worker'),
                  subtitle: Text(widget.name),
                  trailing: Icon(Icons.more_vert),
                ),
              ),
              Card(
                child: ListTile(
                  title: Text('Worker email'),
                  subtitle: Text(widget.email),
                  trailing: Icon(Icons.more_vert),
                ),
              ),
              Card(
                child: ListTile(
                  title: Text('Worker phone'),
                  subtitle: Text(widget.phone),
                  trailing: Icon(Icons.more_vert),
                ),
              ),
              DropdownButton(
                elevation: 16,
                icon: Icon(Icons.arrow_drop_down_circle),
                isExpanded: true,
                items: widget.services.map((e) {
                  return DropdownMenuItem(
                    value: e,
                    child: Text(e),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    service = value;
                  });
                },
                value: service,
              ),
              DropdownButton(
                elevation: 16,
                icon: Icon(Icons.arrow_drop_down_circle),
                isExpanded: true,
                items:
                    ['Tarjeta', 'Efectivo', 'Nequi', 'Transferencia'].map((e) {
                  return DropdownMenuItem(
                    value: e,
                    child: Text(e),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    payment_method = value;
                  });
                },
                value: payment_method,
              ),
              TextFormField(
                controller: _price,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Offered Price',
                    hintText: 'Enter your age'),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          'Choose Date',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.5),
                        ),
                        InkWell(
                          onTap: () {
                            _selectDate(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 30),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(color: Colors.grey[200]),
                            child: TextFormField(
                              style: TextStyle(fontSize: 15),
                              textAlign: TextAlign.center,
                              enabled: false,
                              keyboardType: TextInputType.text,
                              controller: _dateController,
                              onSaved: (String val) {
                                _setDate = val;
                              },
                              decoration: InputDecoration(
                                  disabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide.none),
                                  // labelText: 'Time',
                                  contentPadding: EdgeInsets.only(top: 0.0)),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          'Choose Time',
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.5),
                        ),
                        InkWell(
                          onTap: () {
                            _selectTime(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 30),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(color: Colors.grey[200]),
                            child: TextFormField(
                              style: TextStyle(fontSize: 15),
                              textAlign: TextAlign.center,
                              onSaved: (String val) {
                                _setTime = val;
                              },
                              enabled: false,
                              keyboardType: TextInputType.text,
                              controller: _timeController,
                              decoration: InputDecoration(
                                  disabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide.none),
                                  // labelText: 'Time',
                                  contentPadding: EdgeInsets.all(5)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                child: const Text('Hire'),
                onPressed: () async {
                  bool isConection = await Connectivity_provider().isConetion();
                  if (!isConection) {
                    _showDialog(
                        "No Internet connection. Make sure that Wi-Fi or mobile data is turned on, then try again.");
                  } else {
                    await SharedPreferences.getInstance().then((prefs) async {
                      var resp = prefs.getString('isUser');
                      proposalBloc.postProposal(
                          resp,
                          dateTime,
                          _timeController.text,
                          widget.latitude,
                          widget.longitude,
                          payment_method,
                          _price.text,
                          service,
                          widget.email);
                      _showDialog(
                          "The contract was proposed");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => MyApp(user: resp)));

                    });

                  }
                },
              ),
            ],
          ),
        ));
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:house_holdd/src/resources/connectivity_provider.dart';
import 'package:house_holdd/src/ui/proposal.dart';

class WorkerUI extends StatelessWidget {
  String name;
  String email;
  int age;
  double longitude;
  double latitude;
  String phone;
  bool covid_sympthoms;
  String image_url;
  double distance;
  List services;

  WorkerUI(this.name, this.email, this.age, this.longitude, this.latitude,
      this.phone, this.covid_sympthoms, this.image_url, this.distance,this.services);

  @override
  Widget build(BuildContext context) {
    _showDialog(mensaje) {
      showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
                title: Text(mensaje),
                actions: [
                  CupertinoDialogAction(
                      child: Text('Close'),
                      onPressed: () => Navigator.pop(context)),
                ],
              ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Worker information"),
      ),
      body: Column(
        children: [
          SizedBox(height: 5),
          Row(
            children: [
              CachedNetworkImage(
                imageUrl: image_url,
                width: 100,
                height: 100,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              Text("Hi, I'm " + this.name),
            ],
          ),
          Text("My Contact Information:",
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0
              )),
          ListView(
            shrinkWrap: true,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.email),
                title: Text(email),
              ),
              ListTile(
                leading: Icon(Icons.phone),
                title: Text(phone),
              )
            ],
          ),
          ElevatedButton(
            child: const Text('Hire me'),
            onPressed: () async {
              bool isConection = await Connectivity_provider().isConetion();
              if (!isConection) {
                _showDialog(
                    "No Internet connection. Make sure that Wi-Fi or mobile data is turned on, then try again.");
              } else {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => Proposal(
                            this.name,
                            this.email,
                            this.age,
                            this.longitude,
                            this.latitude,
                            this.phone,
                            this.covid_sympthoms,
                            this.image_url,
                            this.distance,
                        this.services)
                    )
                );
              }
            },
          ),
        ],
      ),
    );
  }
}

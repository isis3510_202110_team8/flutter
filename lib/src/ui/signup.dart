import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:house_holdd/src/blocs/signup_bloc.dart';
import 'package:house_holdd/main.dart';
import 'package:house_holdd/src/resources/connectivity_provider.dart';
import 'package:house_holdd/src/ui/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final fontColor = Color.fromRGBO(59, 89, 152, 5);

  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _age = TextEditingController();
  TextEditingController _first_name = TextEditingController();
  TextEditingController _last_name = TextEditingController();
  TextEditingController _address = TextEditingController();
  String gender = 'Male';

  Widget textField(_controller, title, defaultText,obscure){
    return TextFormField(
      controller: _controller,
      obscureText: obscure,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: title,
          hintText: defaultText,
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    showDialog(mensaje){
      showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
            title: Text(mensaje),
            actions: [
              CupertinoDialogAction(
                  child: Text('Close'),
                  onPressed: () => Navigator.pop(context)
              ),
            ],
          )
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Container(
                  width: 200,
                  height: 80,
                  child: Text(
                    'HouseChores',
                    style: TextStyle(
                        fontFamily: 'PlayfairDisplay',
                        color: fontColor,
                        fontSize: 30),
                  ),
                ),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Form(
                child: Column(
                  children: [
                    textField(_first_name, 'First Name', 'Enter your first name',false),
                    textField(_last_name, 'Last Name', 'Enter your last name',false),
                    textField(_email, 'Email', 'Enter valid email',false),
                    textField(_password, 'Password', 'Enter secure password',true),
                    TextFormField(
                      controller: _age,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Age',
                          hintText: 'Enter your age'),
                    ),
                    Row(

                      children: [
                        Text("Gender"),
                        Container(width: 20),
                        Container(
                          width: 100.0,
                          child: DropdownButton(
                            value: gender,
                              items: [
                                DropdownMenuItem(
                                  child: Text("Male"),
                                  value: "Male",
                                ),
                                DropdownMenuItem(
                                  child: Text("Female"),
                                  value: "Female",
                                ),
                                DropdownMenuItem(
                                    child: Text("Other"),
                                    value: "Other"),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  gender = value;
                                });
                              },
                          ),
                        ),
                      ],
                    ),
                    textField(_address, 'Address', 'Enter your address',false),
                  ],
                ),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: fontColor, borderRadius: BorderRadius.circular(5)),
              child: TextButton(
                onPressed: () async {
                  var addresses;
                  try {

                    bool isConection = await Connectivity_provider().isConetion();
                    if (isConection) {
                      addresses = await Geocoder.local.findAddressesFromQuery(
                          _address.text);
                      var longitude = addresses.first.coordinates.longitude;
                      var latitude = addresses.first.coordinates.latitude;
                      var now = new DateTime.now();
                      var formatter = new DateFormat('dd/MM/yyyy');
                      String formattedDate = formatter.format(now);
                      // I am connected to a mobile or wifi network.
                      signUpBloc
                          .singUP(_email.text, _password.text)
                          .then((value) async {
                        await SharedPreferences.getInstance().then((prefs) {
                          String resp = prefs.getString('isUser');
                          if (resp == "") {
                            FirebaseAuth _auth = FirebaseAuth.instance;
                            resp = _auth.currentUser.email;
                            prefs.setString('isUser', resp);
                          }
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => MyApp(user: resp)));
                        });
                        signUpBloc.insertUser(_first_name.text, _last_name.text, _email.text, int.parse(_age.text), gender, longitude, latitude, formattedDate);
                      }).catchError((err)=>showDialog(err.message));

                    } else {
                      showDialog("No Internet connection. Make sure that Wi-Fi or mobile data is turned on, then try again.");
                    }
                  }
                  on Exception{
                    showDialog("The address is incorrect");
                  }
                },
                child: Text(
                  'Sign Up',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: fontColor, borderRadius: BorderRadius.circular(5)),
              child: TextButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => Login()));
                },
                child: Text(
                  'Log In',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

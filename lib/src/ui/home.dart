import 'dart:async';
import 'package:house_holdd/src/resources/my_connectivity.dart';
import 'package:flutter/material.dart';
import 'package:house_holdd/src/blocs/home_bloc.dart';
import 'package:house_holdd/src/models/categories_model.dart';
import 'package:house_holdd/src/ui/detail.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:house_holdd/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:connectivity/connectivity.dart';
import '../blocs/home_bloc.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MyConnectivity _connectivity = MyConnectivity.instance;
  Map _source = {ConnectivityResult.none: false};

  //Remove repeated code
  _evaluate(source){
    if(_source.keys.first!=source.keys.first){
      _source = source;
      categoriesBloc.calificate().then((value) {
        if (value) {
          _showDialog();
        }
      });
    }
  }

  @override
  void initState() {
    _source = {ConnectivityResult.none: true};
    _connectivity.initialise();
    _connectivity.myStream.listen((source) {
      switch (source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          _evaluate(source);
          break;
        case ConnectivityResult.wifi:
          _evaluate(source);
          break;
        case ConnectivityResult.none:
          if(_source.keys.first!=source.keys.first){
            _source = source;
          }
          break;
      }
    });

  }

  _showDialog() async {
    await Future.delayed(Duration(milliseconds: 50));
    var rating = 0.0;
    // set up the button
    Widget evaluate = SmoothStarRating(
      rating: rating,
      size: 60,
      starCount: 5,
      onRated: (value) {
        setState(() {
          rating = value;
        });
      },
    );

    Widget _builder(mensaje,respuesta) {
      return Builder(
        builder: (context) => TextButton(
          child: Text(mensaje),
          onPressed: () {
            categoriesBloc.sendCalification(respuesta);
            Navigator.of(context).pop();
          },
        ),
      );
    }
    // set up the button
    Widget okButton = Center(
      child: Row(
        children: <Widget>[
          _builder("Later", ""),
          _builder("No", "N/A"),
          _builder("Yes", rating),

        ],
      ),
    );

    Widget resp = Column(
      children: [evaluate, okButton],
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Calificate"),
      content: Text("You want to qualify the last worker?"),
      actions: [resp],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  //CategoriasBloc categoriasBloc = new CategoriasBloc();
  @override
  Widget build(BuildContext context) {
    categoriesBloc.fetchAllCategories();
    return WillPopScope(
      onWillPop: () {},
      child: Scaffold(
        appBar:
            AppBar(title: Text(widget.title), automaticallyImplyLeading: false),
        body: StreamBuilder(
          stream: categoriesBloc.allCategories,
          builder: (context, AsyncSnapshot<CategoriesModel> snapshot) {
            if (snapshot.hasData) {
              return buildList(snapshot);
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            FirebaseAuth.instance.signOut().then((value) async {
              await SharedPreferences.getInstance().then((instance) {
                instance.setString('isUser', "");
              });
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => MyApp(user: "")));
            });
          },
          label: Text('Log Out'),
          icon: Icon(Icons.logout),
          backgroundColor: Colors.pink,
        ),
      ),
    );
  }

  Widget buildList(AsyncSnapshot<CategoriesModel> snapshot) {
    return GridView.builder(
      itemCount: snapshot.data.categories.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3, crossAxisSpacing: 0.0, mainAxisSpacing: 25.0),
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => Detail(
                      category: snapshot.data.categories[index],
                    )));
          },
          child: Column(
            children: <Widget>[
              Image.asset(
                'assets/images/' +
                    snapshot.data.categories[index].name +
                    '.png',
              ),
              Text(snapshot.data.categories[index].name)
            ],
          ),
        );
      },
    );
  }
}

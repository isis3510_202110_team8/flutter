import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:house_holdd/src/resources/connectivity_provider.dart';

class Main_Bloc{
  Future<String> hayTareas() async {
    String resp = "";
    bool isConection = await Connectivity_provider().isConetion();
    if (isConection) {
      var userEmail = FirebaseAuth.instance.currentUser.email;
      final databaseReference = FirebaseDatabase.instance.reference().child("users");

      await databaseReference.once().then((DataSnapshot snapshot) {

        Map mapa = snapshot.value;
        List lista = mapa.values.toList();

        bool encontro = false;
        for(int i = 1;i < lista.length && !encontro;i++){
          if (lista[i]['email']==userEmail){
            encontro = true;
            if(lista[i]['contratos']!=null){
              for(int j = 0;j<lista[i]['contratos'].length;j++){
                String dia = lista[i]['contratos'][j]['dia'];
                var mesDiaAnoFecha = dia.split("/");
                DateTime day = DateTime.utc(int.parse(mesDiaAnoFecha[2]), int.parse(mesDiaAnoFecha[1]), int.parse(mesDiaAnoFecha[0]));
                DateTime now = DateTime.now();
                int dif = day.difference(now).inDays;
                Duration dif2 = day.difference(now);

                if(dif2.inHours>-4 && dif ==0){

                  resp = (lista[i]['contratos'][j]['worker']);
                }
              }
            }
          }
        }
      });
    }


    return resp;
  }
}
final mainBloc = Main_Bloc();
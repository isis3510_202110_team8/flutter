import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc{
  Future singIn(String _email,String _password) async {
    await FirebaseAuth.instance.signInWithEmailAndPassword(
      email: _email,
      password: _password,
    ).then((value)  async {
      await SharedPreferences.getInstance().then((instance){
        instance.setString('isUser', _email);
      });
    }).catchError((e)=>throw(e));


  }
}

final loginBloc = LoginBloc();



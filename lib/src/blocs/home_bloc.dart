import 'package:firebase_auth/firebase_auth.dart';
import 'package:house_holdd/src/resources/connectivity_provider.dart';

import '../resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import '../models/categories_model.dart';
import 'package:firebase_database/firebase_database.dart';

class CategoriesBloc {
  final _repository = Repository();

  final _categoriesFetcher = PublishSubject<CategoriesModel>();

  Stream<CategoriesModel> get allCategories =>_categoriesFetcher.stream;

  fetchAllCategories() async {
    CategoriesModel categoriesModel = await _repository.fetchAllCategories();
    _categoriesFetcher.sink.add(categoriesModel);
  }

  Future<bool> calificate() async {
    bool calificated = false;
    bool isConection = await Connectivity_provider().isConetion();
    if (isConection) {
      var userEmail = FirebaseAuth.instance.currentUser.email;
      final databaseReference = FirebaseDatabase.instance.reference().child("users");
      List listaC = [];
      await databaseReference.once().then((DataSnapshot snapshot) {
        Map mapa = snapshot.value;
        List lista = mapa.values.toList();
        bool encontro = false;
        for(int i = 1;i < lista.length && !encontro;i++){
          if (lista[i]['email']==userEmail){
            encontro = true;
            listaC = lista[i]['contratos'];
            if(listaC!=null) {
              if (listaC[listaC.length - 1]['calificacion'] == "") {
                calificated = true;
              }
            }
          }
        }

      });
    }

    return calificated;
  }

  Future<void> sendCalification(calificate) async {
    var userEmail = FirebaseAuth.instance.currentUser.email;
    final databaseReference = FirebaseDatabase.instance.reference().child("users");

    await databaseReference.once().then((DataSnapshot snapshot) {
      Map mapa = snapshot.value;
      List lista = mapa.values.toList();
      bool encontro = false;
      for(int i = 1;i < lista.length && !encontro;i++){
        if (lista[i]['email']==userEmail){
          encontro = true;
          lista[i]['contratos'][lista[i]['contratos'].length-1]['calificacion'] = calificate;
        }
      }
      databaseReference.set(lista);

    });
  }


  dispose() {
    _categoriesFetcher.close();
  }
}

final categoriesBloc = CategoriesBloc();
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geolocator/geolocator.dart';
import 'package:house_holdd/src/database/database.dart';
import 'package:house_holdd/src/models/worker_model.dart';
import 'package:house_holdd/src/resources/connectivity_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math';
import '../models/categories_model.dart';
import '../models/contract_model.dart';
import '../resources/repository.dart';

class DetailBloc {
  //WorkersDatabase db = WorkersDatabase();
  String category;
  final _repository = Repository();
  final dbHelper = DatabaseHelper.instance;
  double averagePrice = 0;

  DetailBloc(String pCategory) {
    this.category = pCategory;
  }

  //Calculate harvessian distance
  double calculateDistance(lat1, lon1, lat2, lon2) {

    var p = 0.017453292519943295;
    var c = cos;

    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;

    return double.parse((12742 * asin(sqrt(a))).toStringAsFixed(2));
  }



  //Sacar los workers de la categoria
  Future<List> getWorkersCategories() async {
    CategoriesModel categoriesModel =
        await _repository.fetchACategorie(category);
    final List workers = [];

    List workerList = [];
    for(int i = 0; i < categoriesModel.category.length;i++){
      workerList = categoriesModel.category[i].workers;
      for(int j = 0;j < workerList.length;j++){
        workers.add(workerList[j]);
      }
    }



    return workers;
  }

  //DarUbicaciónActual
  Future<Position> _determinePosition() async {
    bool serviceEnabled;

    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    return await Geolocator.getCurrentPosition();
  }

  Stream<List> get getWorkers async* {

    bool isConection = await Connectivity_provider().isConetion();
    if (isConection) {
      _delete(this.category);
      CategoriesModel categorie = await _repository.fetchACategorie(category);

      List<WorkerModel> workers = categorie.category.first.workers;
      double priceSum = 0;
      int priceCount = 0;

      Position position = await _determinePosition();

      double longitude = position.longitude;

      double latitude = position.latitude;

      List workersFinal = [];
      List workersRecommended = [];
      List workersBest = [];
      List workersDistancia = [];
      List workersBad = [];



      //Imprimir información de los workers poniendo los good workers de primeros

      for (var i = 0; i < workers.length; i++) {
        WorkerModel workerModel = workers[i];
        List actualWorker = [];

        double distance = calculateDistance(
            latitude, longitude, workerModel.latitud, workerModel.longitud);


        actualWorker.add(workerModel.firstName);
        actualWorker.add(workerModel.email);
        actualWorker.add(workerModel.age);
        actualWorker.add(workerModel.longitud);
        actualWorker.add(workerModel.latitud);
        actualWorker.add(workerModel.phone);
        actualWorker.add(workerModel.covid_symptoms);
        actualWorker.add(workerModel.image_url);
        actualWorker.add(distance);
        actualWorker.add(workerModel.services);

        String userEmail = "";

        await SharedPreferences.getInstance().then((prefs) {
          userEmail = prefs.getString('isUser');
          if (userEmail == "") {
            FirebaseAuth _auth = FirebaseAuth.instance;
            userEmail = _auth.currentUser.email;
            prefs.setString('isUser', userEmail);
          }
        });

        double calificacionTotal = 0;


        bool isGood = false;
        bool isBad = false;


        if(workerModel.contratos!=null){

          for(int j = 0; j< workerModel.contratos.length ;j++){
            ContractModel actualContract = workerModel.contratos[j];
            priceSum += workerModel.contratos[j].price;
            priceCount ++;

            if(actualContract.calification>=4 && actualContract.clientEmail==userEmail && !isGood && !isBad){
              workersRecommended.add(actualWorker);

              isGood = true;
            }

            else if(actualContract.calification<=1 && actualContract.clientEmail==userEmail && !isGood && !isBad){
              workersBad.add(actualWorker);
              isBad = true;
            }

            calificacionTotal+=actualContract.calification;
          }

          calificacionTotal = calificacionTotal/workerModel.contratos.length;
          if(!isGood && !isBad && calificacionTotal>4.5){
            workersBest.add(actualWorker);
          }

          else if(!isGood && !isBad){
            workersDistancia.add(actualWorker);
          }

        } else {
          workersDistancia.add(actualWorker);
        }

        for(int i = 0; i < workerModel.services.length;i++){
          _insertServices(Service(
              email: workerModel.email,
              service: workerModel.services[i]
          ));
        }

      }
      if(priceSum != 0){
        averagePrice = priceSum/priceCount;
      }
      else{
        averagePrice = 0;
      }


      await SharedPreferences.getInstance().then((prefs) async {
        prefs.setDouble(category, averagePrice);
        double price = prefs.getDouble(category);
      });

      workersDistancia.sort((a, b) => a[2].compareTo(b[2]));
      workersFinal.add(workersRecommended);
      workersFinal.add(workersBest);

      for(int i = 0; i < workersBad.length;i++){
        workersDistancia.add(workersBad[i]);
      }

      workersFinal.add(workersDistancia);


      yield workersFinal;
      int num = 0;

      for(var i = 0; i < workersDistancia.length; i++) {
        Worker worker = Worker(
            id: num,
            name: workersDistancia[i][0],
            email: workersDistancia[i][1],
            age: workersDistancia[i][2],
            longitude: workersDistancia[i][3],
            latitude: workersDistancia[i][4],
            phone: workersDistancia[i][5],
            covid_symptoms: workersDistancia[i][6],
            image_url: workersDistancia[i][7],
            distance: workersDistancia[i][8],
            type: 0,
            category: this.category
        );
        num++;
        _insert(worker);
      }
      for(var i = 0; i < workersBest.length; i++) {
        Worker worker = Worker(
            id: num,
            name: workersBest[i][0],
            email: workersBest[i][1],
            age: workersBest[i][2],
            longitude: workersBest[i][3],
            latitude: workersBest[i][4],
            phone: workersBest[i][5],
            covid_symptoms: workersBest[i][6],
            image_url: workersBest[i][7],
            distance: workersBest[i][8],
            type: 1,
            category: this.category
        );
        num++;
        _insert(worker);
      }
      for(var i = 0; i < workersRecommended.length; i++) {
        Worker worker = Worker(
            id: num,
            name: workersRecommended[i][0],
            email: workersRecommended[i][1],
            age: workersRecommended[i][2],
            longitude: workersRecommended[i][3],
            latitude: workersRecommended[i][4],
            phone: workersRecommended[i][5],
            covid_symptoms: workersRecommended[i][6],
            image_url: workersRecommended[i][7],
            distance: workersRecommended[i][8],
            type: 2,
            category: this.category
        );
        num++;
        _insert(worker);
      }
    }
    else{
      List workersFinal = [];
      final allRows = await dbHelper.queryAllRows(category);

      if(allRows.length == 0){
        workersFinal.add("We have no data currently for this category. Please check your connectivity.");
        yield workersFinal;
      }
      else {
        List workersRecommended = [];
        List workersBest = [];
        List workersDistancia = [];
        List workersBad = [];
        for (var i = 0; i < allRows.length; i++) {
          final services = await dbHelper.queryAllRowsServices(allRows[i].email);

          List data = [
            allRows[i].name,
            allRows[i].email,
            allRows[i].age,
            allRows[i].longitude,
            allRows[i].latitude,
            allRows[i].phone,
            allRows[i].covid_symptoms,
            allRows[i].image_url,
            allRows[i].distance,
            List.generate(services.length, (i) {
              return services[i].service.toString();
            })
          ];
          if (allRows[i].type == 0) {
            workersDistancia.add(data);
          }
          else if (allRows[i].type == 1) {
            workersBest.add(data);
          }
          else if (allRows[i].type == 2) {
            workersRecommended.add(data);
          }
        }
        workersFinal.add(workersRecommended);
        workersFinal.add(workersBest);
        workersFinal.add(workersDistancia);
        yield workersFinal;
      }
    }
  }

  Future<void> _insert(Worker worker) async {
    // row to insert
    Map<String, dynamic> row = {
      DatabaseHelper.columnName : worker.name,
      DatabaseHelper.columnEmail  : worker.email,
      DatabaseHelper.columnDistance  : worker.distance,
      DatabaseHelper.columnAge  : worker.age,
      DatabaseHelper.columnLongitude  : worker.longitude,
      DatabaseHelper.columnLatitude  : worker.latitude,
      DatabaseHelper.columnPhone  : worker.phone,
      DatabaseHelper.columnCovidSymptoms  : worker.covid_symptoms,
      DatabaseHelper.columnImageUrl  : worker.image_url,
      DatabaseHelper.columnType  : worker.type,
      DatabaseHelper.columnCategory  : worker.category
    };
    final id = await dbHelper.insert(row);
  }

  Future<void> _insertServices(Service service) async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnServicesEmail : service.email,
      DatabaseHelper.columnServicesService  : service.service
    };
    final id = await dbHelper.insertService(row);
    var email = service.email;
  }

  /*Stream<List> _query(String category) async {


  }*/

  void _delete(String category) async {
    // Assuming that the number of rows is the id for the last row.
    final id = await dbHelper.queryRowCount();
    final rowsDeleted = await dbHelper.delete(category);
  }

  void _deleteServices(String email) async {
    // Assuming that the number of rows is the id for the last row.
    final rowsDeleted = await dbHelper.deleteServices(email);
  }
}

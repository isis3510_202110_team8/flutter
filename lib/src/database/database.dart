import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class Worker {
  int id;
  String name;
  String email;
  int age;
  double longitude;
  double latitude;
  String phone;
  bool covid_symptoms;
  String image_url;
  double distance;
  int type;
  String category;

  Worker({this.id, this.name, this.email, this.age, this.longitude,
    this.latitude, this.phone, this.covid_symptoms, this.image_url, this.distance,
    this.type, this.category});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'email': email,
      'age': age,
      'longitude': longitude,
      'latitude':latitude,
      'phone': phone,
      'covid_symptoms': covid_symptoms,
      'image_url': image_url,
      'distance': distance,
      'type': type,
      'category': category
    };
  }

  Worker.fromMap(Map<String, dynamic> map){
    id = map['id'];
    name = map['name'];
    email = map['email'];
    age = map['age'];
    longitude= map['longitude'];
    latitude = map['latitude'];
    phone = map['phone'];
    covid_symptoms = map['covid_symptoms'];
    image_url = map['image_url'];
    distance = map['distance'];
    type = map['type'];
    category = map['category'];
  }
}

class Service {
  String email;
  String service;

  Service({this.email, this.service});

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'service': service
    };
  }

  Service.fromMap(Map<String, dynamic> map){
    email = map['email'];
    service = map['service'];
  }
}

class DatabaseHelper {

  static final _databaseName = "household.db";
  static final _databaseVersion = 1;

  static final table = 'workers_new';
  static final tableServices = 'services';

  static final columnId = '_id';
  static final columnName = 'name';
  static final columnEmail = 'email';
  static final columnAge = 'age';
  static final columnLongitude = 'longitude';
  static final columnLatitude = 'latitude';
  static final columnPhone = 'phone';
  static final columnCovidSymptoms = 'covid_symptoms';
  static final columnImageUrl = 'image_url';
  static final columnDistance = 'distance';
  static final columnType = 'type';
  static final columnCategory = 'category';

  static final columnServicesEmail = 'email';
  static final columnServicesService = 'service';

  // make this a singleton class
  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // only have a single app-wide reference to the database
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      _onCreate(_database, _databaseVersion);
      return _database;
    }
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }


  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE IF NOT EXISTS $table (
            $columnId INTEGER PRIMARY KEY,
            $columnName TEXT NOT NULL,
            $columnEmail TEXT NOT NULL,
            $columnAge INTEGER NOT NULL,
            $columnLongitude REAL NOT NULL,
            $columnLatitude REAL NOT NULL,
            $columnPhone TEXT NOT NULL,
            $columnCovidSymptoms BOOL NOT NULL,
            $columnImageUrl TEXT NOT NULL,
            $columnDistance REAL NOT NULL,
            $columnType INTEGER NOT NULL,
            $columnCategory TEXT NOT NULL
          )
          ''');

    await db.execute('''
          CREATE TABLE IF NOT EXISTS $tableServices (
            $columnServicesEmail TEXT NOT NULL,
            $columnServicesService TEXT NOT NULL
          )
          ''');
  }

  // Helper methods

  // Inserts a row in the database where each key in the Map is a column name
  // and the value is the column value. The return value is the id of the
  // inserted row.
  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<int> insertService(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(tableServices, row);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Worker>> queryAllRows(String category) async {
    Database db = await instance.database;
    final List<Map<String, dynamic>> workers = await db.query(table,
        where: 'category = ?',
        whereArgs: [category]
    );
    return List.generate(workers.length, (i) {
      bool covid_symptoms = true;
      if (workers[i]['covid_symptoms'] == 1)
        covid_symptoms = false;
      else
        covid_symptoms = true;
      return Worker(
        id: workers[i]['id'],
        name: workers[i]['name'],
        email: workers[i]['email'],
        age: workers[i]['age'],
        longitude: workers[i]['longitude'],
        latitude: workers[i]['latitude'],
        phone: workers[i]['phone'],
        covid_symptoms: covid_symptoms,
        image_url: workers[i]['image_url'],
        distance: workers[i]['distance'],
        type: workers[i]['type'],
        category: workers[i]['category'],
      );
    });
  }

  Future<List<Service>> queryAllRowsServices(String email) async {
    Database db = await instance.database;
    final List<Map<String, dynamic>> services = await db.query(tableServices,
        where: 'email = ?',
        whereArgs: [email]
    );
    return List.generate(services.length, (i) {

      return Service(
        email: services[i]['email'],
        service: services[i]['service']
      );
    });
  }


  // All of the methods (insert, query, update, delete) can also be done using
  // raw SQL commands. This method uses a raw query to give the row count.
  Future<int> queryRowCount() async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $table'));
  }

  // We are assuming here that the id column in the map is set. The other
  // column values will be used to update the row.
  Future<int> update(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int id = row[columnId];
    return await db.update(table, row, where: '$columnId = ?', whereArgs: [id]);
  }

  // Deletes the row specified by the id. The number of affected rows is
  // returned. This should be 1 as long as the row exists.
  Future<int> delete(String category) async {
    Database db = await instance.database;
    return await db.delete(table, where: 'category = ?', whereArgs: [category]);
  }

  Future<int> deleteServices(String email) async {
    Database db = await instance.database;
    return await db.delete(tableServices, where: 'email = ?', whereArgs: [email]);
  }
}

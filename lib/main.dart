import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:house_holdd/src/ui/login.dart';
import 'package:house_holdd/src/ui/home.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rxdart/subjects.dart' as rxSub;
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:house_holdd/src/blocs/main_bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:connectivity/connectivity.dart';
import 'package:house_holdd/src/resources/my_connectivity.dart';

NotificationAppLaunchDetails notifLaunch;
final FlutterLocalNotificationsPlugin notifsPlugin =
    FlutterLocalNotificationsPlugin();

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await Firebase.initializeApp();
  String resp;
  await SharedPreferences.getInstance().then((prefs) async {
    resp = prefs.getString('isUser');
    if (resp == null) {
      FirebaseAuth _auth = FirebaseAuth.instance;
      var resp2 = _auth.currentUser;

      if (resp2 == null) {
        prefs.setString('isUser', "");
        resp = "";
      } else {
        prefs.setString('isUser', _auth.currentUser.email);
        resp = _auth.currentUser.email;
      }
    }
  });

  runApp(MyApp(user: resp));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  MyApp({Key key, this.user}) : super(key: key);


  final String user;

  @override
  _MyAppState createState() => _MyAppState();
}

class NotificationClass {
  final int id;
  final String title;
  final String body;
  final String payload;

  NotificationClass({this.id, this.body, this.payload, this.title});
}

class _MyAppState extends State<MyApp> {
  MyConnectivity _connectivity = MyConnectivity.instance;

  final rxSub.BehaviorSubject<NotificationClass>
      didReceiveLocalNotificationSubject =
      rxSub.BehaviorSubject<NotificationClass>();
  final rxSub.BehaviorSubject<String> selectNotificationSubject =
      rxSub.BehaviorSubject<String>();

  Future<void> initNotifications(
      FlutterLocalNotificationsPlugin notifsPlugin) async {
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    var initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);
    await notifsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
      selectNotificationSubject.add(payload);
    });
  }

  Future<void> scheduleNotification(
      {FlutterLocalNotificationsPlugin notifsPlugin,
      String id,
      String title,
      String body,
      DateTime scheduledTime,
      String messages}) async {
    tz.initializeTimeZones();
    var androidSpecifics = AndroidNotificationDetails(
      id, // This specifies the ID of the Notification
      'Scheduled notification',
      // This specifies the name of the notification channel
      'A scheduled notification',
      //This specifies the description of the channel
      icon: '@mipmap/ic_launcher',
    );
    var platformChannelSpecifics =
        NotificationDetails(android: androidSpecifics);
    await notifsPlugin.zonedSchedule(
        0,
        title,
        'Tienes trabajos con el worker: ' + messages,
        tz.TZDateTime.from(scheduledTime, tz.local),
        platformChannelSpecifics,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime);
  }

  ThemeData _selectedTheme;

  ThemeData _lightTheme = ThemeData(
    primarySwatch: Colors.blue,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    brightness: Brightness.light,
  );

  ThemeData _darkTheme = ThemeData(
    primarySwatch: Colors.brown,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    brightness: Brightness.dark,
  );

  /// Timer will be required to change the theme after some time.
  Timer _timer;

  _selectTheme() {
    // Current time
    DateTime now = DateTime.now();
    // Dark theme start time evening 7 pm
    DateTime darkThemeStartTime = DateTime(now.year, now.month, now.day, 19);
    // Dark theme to Light switch happen at 6 am
    DateTime darkThemeEndTime = DateTime(now.year, now.month, now.day, 6);

    // time left to change the theme
    int timerSeconds;

    // if current time is after 6 am & before 7 pm then light theme else dark theme
    if (now.compareTo(darkThemeEndTime) > 0 &&
        now.compareTo(darkThemeStartTime) < 0) {
      _selectedTheme = _lightTheme;
      timerSeconds = darkThemeStartTime.difference(now).inSeconds;
    } else if (now.compareTo(darkThemeStartTime) > 0) {
      _selectedTheme = _darkTheme;
      DateTime nextDayMorningTime = darkThemeStartTime.add(Duration(days: 1));
      timerSeconds = nextDayMorningTime.difference(now).inSeconds;
    } else {
      _selectedTheme = _darkTheme;
      timerSeconds = darkThemeEndTime.difference(now).inSeconds;
    }
    _timer = Timer(
      Duration(seconds: timerSeconds),
      () {
        //_selectTheme will be called after speciefied time
        _selectTheme();
        setState(() {});
      },
    );
  }

  //Remove repeated code
  _pushNotification(){
    DateTime now = DateTime.now();
    // Dark theme start time evening 7 pm
    DateTime notificationTime = DateTime(now.year, now.month, now.day, 22, 0);
    if (widget.user != "") {
      mainBloc.hayTareas().then((value) {
        Duration dif = notificationTime.difference(now);
        if (value != "" && dif.inMilliseconds > 0) {
          scheduleNotification(
              notifsPlugin: notifsPlugin,
              id: DateTime.now().toString(),
              body: "A scheduled Notification",
              scheduledTime: notificationTime,
              messages: value);
        }
      });
    }
  }

  @override
  void initState() {
    _selectTheme();
    initNotifications(notifsPlugin);
    _connectivity.initialise();
    _connectivity.myStream.listen((source) {
      switch (source.keys.toList()[0]) {
        case ConnectivityResult.mobile:
          _pushNotification();
          break;
        case ConnectivityResult.wifi:
          _pushNotification();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  Widget resp(metod){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'House Chores',
      theme: _selectedTheme,
      home: metod,
    );
  }

  @override
  Widget build(BuildContext context) {
    if (widget.user == "") {
      return resp(Login());
    } else {
      return resp(MyHomePage(title: 'HouseChores'));
    }
  }
}
